import sqlite3
import os
import pandas as pd
import numpy as np
os.chdir('/Users/prajwalshreyas/Desktop/Singularity/dockerApps/app_face_rec/')
import face_recognition
from config import *

# path to databse for storing known faces
face_id_list = '/app_face_recognition/known_faces/face_id_list.csv'
database = '/app_face_recognition/database/video_metadata.db'
table_name = 'known_faces'

#Function 1) Create or open database connection
def create_or_open_db(database):
    '''This either creates the main database or opens it'''
    file_exists = os.path.isfile(database)
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    if file_exists:
        #print ''' "{}" database successfully opened '''.format(filename)
        fix_break =1
    else:
        print(''' "{}" database successfully created '''.format(database))
    return conn

# Function 2)  Create table inside the database
def create_table(conn, database,table_name):

    cursor = conn.cursor()
    #This command deletes the table should it already exist
    #This is to avoid the table filling becoming cluttered and un-ordered
    #cursor.execute('''DROP TABLE IF EXISTS ''' + table_name)

    #Inserts the table
    cursor.execute(''' CREATE TABLE IF NOT EXISTS ''' + table_name + '''(PersonName TEXT, pic_path TEXT, image_encoding float, PRIMARY KEY (PersonName, pic_path))''')

    print(''' Table "{}"  successfully created '''.format(table_name))


# Function 3) Insert video metadata into database table
def insert_kown_faces(face_id_list, conn, database,table_name):

    df_face_list = pd.read_csv(face_id_list)
    known_faces_list = []
    # Load some sample pictures and learn how to recognize them.
    for index, row in df_face_list.iterrows():
        name = (row["Name"])
        pic_path = (row["path"])

        load_image = face_recognition.load_image_file(pic_path)
        load_face_encoding = face_recognition.face_encodings(load_image)[0]

        # insert into df
        face_list = [name, pic_path, load_face_encoding]
        known_faces_list.append(face_list)

    df_known_faces = pd.DataFrame(known_faces_list, columns = ['PersonName','pic_path','image_encoding'])
    df_known_faces['image_encoding'] = df_known_faces['image_encoding'].astype(str)

    # insert data to sql
    df_known_faces.to_sql(table_name, conn, index = False, if_exists='replace')
    conn.commit()
    conn.close()

# main function
def main():
    conn = create_or_open_db(database)
    create_table(conn, database, table_name)
    insert_kown_faces(face_id_list, conn, database, table_name)
