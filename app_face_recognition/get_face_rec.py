import os
import glob
os.chdir('/Users/prajwalshreyas/Desktop/Singularity/dockerApps/app_face_rec')
import app_face_recognition as face_recognition
import cv2
import numpy as np
import sqlite3
import pandas as pd
import time
import dlib
# This is a demo of running face recognition on a video file and saving the results to a new video file.
#
# PLEASE NOTE: This example requires OpenCV (the `cv2` library) to be installed only to read from your webcam.
# OpenCV is *not* required to use the face_recognition library. It's only required if you want to run this
# specific demo. If you have trouble installing it, try any of the other demos that don't require it instead.


# initialise variables

database = "/app_face_recognition/database/video_metadata.db"
# add faces to the know faces list
table_name_kf = 'known_faces'
tablename_res = 'video_logs_fd'

# Function 1) Create or open database connection
def create_or_open_db(database):
    '''This either creates the main database or opens it'''
    file_exists = os.path.isfile(database)
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    if file_exists:
        #print ''' "{}" database successfully opened '''.format(filename)
        fix_break =1
    else:
        print(''' "{}" database successfully created '''.format(database))
    return conn


def read_table(conn, table_name_kf, database):

    # picks up the data from the csv and inserts it into a panda dataframe
    cursor = conn.cursor()
    cursor.execute('''SELECT * FROM ''' + table_name_kf)
    table_list = cursor.fetchall()
    table_df = pd.DataFrame(table_list)
    conn.close()

    return table_df


def insert_results(fr_res, tablename_res):

    conn = create_or_open_db(database)

    # insert data to sql
    fr_res.to_sql(tablename_res, conn, index = False, if_exists='append')

    conn.commit()
    conn.close()


# Function 3) Insert guest face encoding into df
def insert_guest(name, face_encoding):

    conn = create_or_open_db(database)
    df_known_faces = pd.DataFrame(columns = ['PersonName','pic_path','image_encoding'])

    df_known_faces.loc[0,'PersonName'] = name
    df_known_faces.loc[0,'pic_path'] = "path/"+name
    df_known_faces.loc[0,'image_encoding'] = str(face_encoding)

    # insert data to sql
    df_known_faces.to_sql(table_name_kf, conn, index = False, if_exists='append')

    conn.commit()
    conn.close()


def get_faceRec(detected, img_h, img_w, baseImage, fid):

    fr_out_all = {}
    dlib_face_locations = []
    img_size = 64

     # holds faces for the age detection functions
    faces = np.empty((len(detected), img_size, img_size, 3))

    # get known faces encoding
    conn = create_or_open_db(database)
    known_faces_df = read_table(conn, table_name_kf, database)
    face = known_faces_df[2][0]

    # parse face encoding
    known_faces_df[2] = known_faces_df[2].apply(lambda x:
                               np.fromstring(
                               x.replace('\n','')
                                .replace('[','')
                                .replace(']','')
                                .replace('  ',' '), sep=' '))

    known_faces = known_faces_df[2].tolist()
    face_labels = known_faces_df[0].tolist()

    print (face_labels)


    for i, d in enumerate(detected):
        x1, y1, x2, y2, w, h = d.left(), d.top(), d.right() + 1, d.bottom() + 1, d.width(), d.height()

        # generate facelocatios for recognition
        dlib_face_loc_tuple = (y1, x2, y2, x1)
        dlib_face_locations.append(dlib_face_loc_tuple)


        #print ("Dlib coordinates {}".format(dlib_face_locations))
        #generate faces for age gender detection
        xw1 = max(int(x1 - 0.4 * w), 0)
        yw1 = max(int(y1 - 0.4 * h), 0)
        xw2 = min(int(x2 + 0.4 * w), img_w - 1)
        yw2 = min(int(y2 + 0.4 * h), img_h - 1)
        cv2.rectangle(baseImage, (x1, y1), (x2, y2), (255, 0, 0), 2)
        faces[i,:,:,:] = cv2.resize(baseImage[yw1:yw2 + 1, xw1:xw2 + 1, :], (img_size, img_size))


    # Find all the faces and face encodings in the current frame of video
    #face_locations = face_recognition.face_locations(frame_rotate)
    print (len(dlib_face_locations))
    if len(dlib_face_locations)>=1:
        face_encodings = face_recognition.face_encodings(baseImage, dlib_face_locations)

        face_names = []
        face_similarity = []
        print (face_encodings)
        #<<<<<<<<<<<<<<<<<<<<<<< Face Recognition >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        for face_encoding in face_encodings:
            # See if the face is a match for the known face(s)

            distance_score = face_recognition.face_distance(known_faces, face_encoding)
            face_index = np.argmin(distance_score)#[-1:0]

            if distance_score[face_index]<0.5:
                    name = face_labels[face_index]
                    face_similarity_score = round((1 - distance_score[face_index]), 2)

                    #update identified names to a list
                    face_names.append(name)
                    face_similarity.append(face_similarity_score)



                    # print(name)

            elif distance_score[face_index]>0.70:
                    #time_now = str(int(time.time()))[-3:]
                    guest_count = guest_count + 1
                    print (guest_count)
                    name = "Guest " + str((guest_count))
                    face_similarity_score = 0

                    # insert the guest name into the sql db
                    insert_guest(name, face_encoding)

                    #append the face encoding and label to the list
                    known_faces.append(face_encoding)
                    face_labels.append(name)

                    #update identified names to a list
                    face_names.append(name)
                    face_similarity.append(face_similarity_score)



            #match = face_recognition.compare_faces(known_faces, face_encoding, tolerance=0.50)



        # Label the results
        for (top, right, bottom, left), name, similarity, age_gender in zip(dlib_face_locations, face_names, face_similarity, age_gender_list):

            fr_out = name+"("+str(similarity)+")"+"- "+str(int(age_gender[0]))+" "+age_gender[1]
            print (fr_out)
            fr_out_all[ fid ] = fr_out

    return fr_out_all
