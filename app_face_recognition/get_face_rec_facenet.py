import os
#os.chdir('/app_face_recognition/images/ip_video/')
#import face_recognition
import cv2
import numpy as np
#import sqlite3
import pandas as pd
import time
import dlib
import boto3
from boto3.s3.transfer import S3Transfer
import tensorflow as tf
from scipy import misc
from os.path import join as pjoin
import sys
import copy
import math
import pickle
import detect_face as detect_face
import pymysql
from config import *
import facenet.facenet_master.src.facenet as facenet
import logging

#rds settings
rds_host  = "sqlcam.c3cxjwruoslb.us-east-1.rds.amazonaws.com"
rds_name = db_username
rds_password = db_password
db_name1 = db_name1
db_name2 = db_name2

#S3 settings
s3_transfer = S3Transfer(boto3.client('s3', region_name='us-east-1',aws_access_key_id = AWS_prajwal_credetials['aws_access_key_id'], aws_secret_access_key = AWS_prajwal_credetials['aws_secret_access_key']))

def connect_db(db_name):
    conn = pymysql.connect(rds_host, user=rds_name, passwd=rds_password, db=db_name, connect_timeout=30)
    return conn

def update_train_flag(train_flag_date):
    # Write new log into database --------
    conn = connect_db(db_name2)
    try:
        with conn.cursor() as cur:
            cur.execute("UPDATE face_rec_train_logs SET train_flag = 'False' WHERE (timestamp = %s)", (train_flag_date))
            conn.commit()
            conn.close()
            status = 'Successfully Updated logs into DB'
    except Exception as e:
            status = e
    return status

# <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Load Face net parameters >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
print('Creating networks and loading parameters')
with tf.Graph().as_default():
    #gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.6)
    #sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options, log_device_placement=False))
    sess = tf.Session()
    with sess.as_default():
        pnet, rnet, onet = detect_face.create_mtcnn(sess, '/app_face_recognition/facenet/facenet_master/src/align/')

        minsize = 20 # minimum size of face
        threshold = [ 0.6, 0.7, 0.7 ]  # three steps's threshold
        factor = 0.709 # scale factor
        margin = 44

        baseImage_interval = 1
        batch_size = 1
        image_size = 182
        input_image_size = 160
        size = (1280, 720)
        #size = (800, 450)
        #size = (1920, 1080)

        print('Loading feature extraction model')
        modeldir = '/app_face_recognition/facenet/facenet_master/src/models/20170512-110547/20170512-110547.pb'
        facenet.load_model(modeldir)

        images_placeholder = tf.get_default_graph().get_tensor_by_name("input:0")
        embeddings = tf.get_default_graph().get_tensor_by_name("embeddings:0")
        phase_train_placeholder = tf.get_default_graph().get_tensor_by_name("phase_train:0")
        embedding_size = embeddings.get_shape()[1]


        # classifier_filename = '/app_face_recognition/facenet/facenet_master/src/my_classifier/my_classifier.pkl'
        # classifier_filename_exp = os.path.expanduser(classifier_filename)
        # with open(classifier_filename_exp, 'rb') as infile:
        #     (model, class_names) = pickle.load(infile)
        #     print('load classifier file-> %s' % classifier_filename_exp)

# def download_s3(s3_path, local_path, bucket='face.rec.training'):
#
#     s3 = boto3.client('s3', region_name='us-east-1',aws_access_key_id = AWS_prajwal_credetials['aws_access_key_id'], aws_secret_access_key = AWS_prajwal_credetials['aws_secret_access_key'])
#     s3.download_file(bucket, s3_path, local_path)

def download_s3(s3_path, local_path, bucket='face.rec.training'):
    s3_transfer.download_file(bucket, s3_path, local_path)
    logging.warning('download_s3')



def get_faceRec(x, y, w, h, baseImage, email_id):

    fr_out_name_all = {}
    fr_out_confidence_all = {}
    dlib_face_locations = []
    img_size = 64
    baseImage_interval = 1
    face_names = []
    face_similarity = []
    fr_flag = {}
    rec_flag = False

    x1 = x
    y1 = y

    x2 = x + w
    y2 = y + h

    dlib_face_locations = [x1, y1, x2, y2]
    #print ("Dlib coordinates {}".format(dlib_face_locations))

    # generate facelocations for recognition
    dlib_face_loc_tuple = (x1, y1, x2, y2)


    logging.warning(dlib_face_locations)
    logging.warning("x1:{}, y1:{}, x2:{}, y2:{}, w:{}, h:{}".format(x1, y1, x2, y2, w, h))

    bounding_boxes = dlib_face_locations
    #<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Load Classifier Model >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    # download latest model from s3
    # if train_flag:
    #     update_train_flag(train_flag_date)
    logging.warning('Start of download from s3')
    logging.warning(email_id)
    #s3_transfer.download_file('face.rec.training', email_id+'/my_classifier.pkl', 'facenet/my_classifier/my_classifier.pkl')
    download_s3(email_id+'/my_classifier.pkl', 'facenet/my_classifier/my_classifier.pkl', 'face.rec.training')
    classifier_filename = '/app_face_recognition/facenet/my_classifier/my_classifier.pkl'
    classifier_filename_exp = os.path.expanduser(classifier_filename)
    with open(classifier_filename_exp, 'rb') as infile:
        (model, class_names) = pickle.load(infile)
        logging.warning('load classifier file-> %s' % classifier_filename_exp)
    #<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Facenet implementation >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    det = bounding_boxes
    #print ("det Box {}".format(det))
    cropped = []
    scaled = []
    scaled_reshape = []
    inner_flag = False
    bb = np.zeros((1,4), dtype=np.int32)
    inner_count = 0
    j = 0
    i = 0
    #for i in range(nrof_faces):
    emb_array = np.zeros((1, embedding_size))

    bb[i][0] = det[0]
    bb[i][1] = det[1]
    bb[i][2] = det[2]
    bb[i][3] = det[3]

    #print ("Bounded Box {}".format(bb))
    # inner exception
    if bb[i][0] <= 0 or bb[i][1] <= 0 or bb[i][2] >= size[0] or bb[i][3] >= size[1]:
        print('face is inner of range!')
        fr_out_name_all = ""
        fr_out_confidence_all = 0
        rec_flag = False
        fr_flag= rec_flag

    else:

        cropped.append(baseImage[bb[i][1]:bb[i][3], bb[i][0]:bb[i][2], :])
        #print ("Inner Range i:{}, cropped:{}".format(i, len(cropped)))
        cropped[j] = facenet.flip(cropped[j], False)
        scaled.append(misc.imresize(cropped[j], (image_size, image_size), interp='bilinear'))
        scaled[j] = cv2.resize(scaled[j], (input_image_size,input_image_size), interpolation=cv2.INTER_CUBIC)
        scaled[j] = facenet.prewhiten(scaled[j])
        scaled_reshape.append(scaled[j].reshape(-1,input_image_size,input_image_size,3))

        #print ("Input feed Dict {}".format(scaled_reshape[i]))
        feed_dict = {images_placeholder: scaled_reshape[j], phase_train_placeholder: False}
        emb_array[0, :] = sess.run(embeddings, feed_dict=feed_dict)
        predictions = model.predict_proba(emb_array)
        best_class_indices = np.argmax(predictions, axis=1)
        best_class_probabilities = predictions[np.arange(len(best_class_indices)), best_class_indices]
        cv2.rectangle(baseImage, (bb[i][0], bb[i][1]), (bb[i][2], bb[i][3]), (0, 255, 0), 2)    #boxing face
        confidence = np.around((best_class_probabilities[0]),2)

        #plot result idx under box
        text_x = bb[i][0]
        text_y = bb[i][3] + 20

        # print('result: ', best_class_indices[0])

        confidence_str = str(confidence)
        ########## download Human Names csv ##########################
        s3_path_names = email_id+'/human_names.csv'
        path = ''
        local_path_names = path+'/app_face_recognition/facenet/my_classifier/human_names.csv'
        download_s3(s3_path_names, local_path_names, bucket='face.rec.training')
        HumanNames = pd.read_csv(local_path_names)['0'].tolist()
        HumanNames = ['Anusha','Prajwal','Ujwal']    #train human name

        for H_i in HumanNames:
            print("Identified Faces {} / {}".format(H_i, HumanNames[best_class_indices[0]]))
            if HumanNames[best_class_indices[0]] == H_i:
                if (confidence)>0.75:
                    rec_flag = True
                    result_names = HumanNames[best_class_indices[0]]
                    output_label = str(result_names +" ("+confidence_str+")")
                    face_names.append(result_names)
                    face_similarity.append(confidence_str)

                    fr_out_name = result_names
                    fr_out_confidence = confidence_str

                    fr_out_name_all = fr_out_name
                    fr_out_confidence_all = fr_out_confidence

                    fr_flag = rec_flag
                    cv2.putText(baseImage, output_label, (text_x, text_y), cv2.FONT_HERSHEY_COMPLEX_SMALL,1, (0, 0, 255), thickness=1, lineType=2)

                    print (fr_out_name_all)
                    print (fr_flag)

                else:

                    result_names = "Guest"
                    rec_flag = False
                    face_names.append(result_names)
                    face_similarity.append(confidence_str)

                    #Saving filtered image to new file
                    file_name = '/app_face_recognition/GuestImages/'+result_names +'.jpg'
                    cv2.imwrite(file_name,baseImage)


                    fr_out_name = result_names
                    fr_out_confidence = confidence_str

                    fr_out_name_all = fr_out_name
                    fr_out_confidence_all = fr_out_confidence

                    fr_flag = rec_flag


    return fr_out_name_all, fr_out_confidence_all
