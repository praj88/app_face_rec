"""Performs face alignment and stores face thumbnails in the output directory."""
# MIT License
#
# Copyright (c) 2016 David Sandberg
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from scipy import misc
import sys
import os
import argparse
import tensorflow as tf
import numpy as np
import random
from time import sleep
# os.chdir('/Users/prajwalshreyas/Desktop/Singularity/dockerapps/app_face_rec/app_face_recognition')
from config import *
os.chdir('/app_face_recognition/facenet/facenet-master/src')
# os.chdir('/Users/prajwalshreyas/Desktop/Singularity/dockerapps/app_face_rec/app_face_recognition/facenet/facenet_master/src')
import align.detect_face
import facenet
import boto3
from boto3.s3.transfer import S3Transfer
from boto3.s3.transfer import TransferConfig
from boto.s3.connection import S3Connection
import logging

logging.basicConfig(stream=sys.stderr, level=logging.WARNING)

#S3 setting
s3 = boto3.client('s3')
s3_transfer = S3Transfer(boto3.client('s3', region_name='us-east-1',aws_access_key_id = AWS_prajwal_credetials['aws_access_key_id'], aws_secret_access_key = AWS_prajwal_credetials['aws_secret_access_key']))
bucket_img  = s3_buckets['img_bucket']

def upload_s3(dirpath, bucket, account_email):
    print('Start of Upload')
    out_path = dirpath#+'/'+account_email
    for root_o,dirs_o,files_o in os.walk(out_path):
            print(root_o)
            print(files_o)
            for file_o in files_o:
                filepath = root_o +'/'+file_o
                sub_dir = os.path.split(os.path.dirname(filepath))[1]

                if "@" not in str(sub_dir) and sub_dir!='processedImages':
                    filepath_s3 = os.path.join(account_email, 'processedImages', sub_dir, file_o)
                else:
                    filepath_s3 = os.path.join(account_email, 'processedImages', file_o)

                print(filepath_s3)
                s3 = boto3.client('s3', region_name='us-east-1',aws_access_key_id = AWS_prajwal_credetials['aws_access_key_id'], aws_secret_access_key = AWS_prajwal_credetials['aws_secret_access_key'])
                s3.upload_file(Bucket='face.rec.training', Key=filepath_s3, Filename=filepath)
    logging.warning('images uploaded to S3')



def download_dir(client, resource, dist, local, bucket='face.rec.training'):
    paginator = client.get_paginator('list_objects')
    for result in paginator.paginate(Bucket=bucket, Delimiter='/', Prefix=dist):
        if result.get('CommonPrefixes') is not None:
            for subdir in result.get('CommonPrefixes'):
                for files in paginator.paginate(Bucket='face.rec.training', Delimiter='/', Prefix=subdir.get('Prefix')):
                    for file in files.get('Contents', []):
                        dest_pathname = os.path.join(local, file.get('Key'))
                        dir_path = os.path.dirname(dest_pathname)
                        print('Dest:{}'.format(dir_path))
                        if not os.path.exists(dir_path):
                            os.makedirs(dir_path)
                        dest_pathname = os.path.join(local, file.get('Key'))
                        resource.meta.client.download_file(bucket, file.get('Key'), dest_pathname)


def _start(local_path, account_email):
    client = boto3.client('s3', region_name='us-east-1',aws_access_key_id = AWS_prajwal_credetials['aws_access_key_id'], aws_secret_access_key = AWS_prajwal_credetials['aws_secret_access_key'])
    resource = boto3.resource('s3')
    download_dir(client, resource, account_email+'/inputImages/', local_path)

_start(local_path, account_email)

def main(input_dir, output_dir, margin, image_size, random_order, detect_multiple_faces):
    sleep(random.random())
    output_dir = os.path.expanduser(output_dir)
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # Store some git revision info in a text file in the log directory
    #src_path,_ = os.path.split(os.path.realpath(__file__))
    #facenet.store_revision_info(src_path, output_dir, ' '.join(sys.argv))
    dataset = facenet.get_dataset(input_dir)

    print('Creating networks and loading parameters')

    with tf.Graph().as_default():
        #gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=args.gpu_memory_fraction)
        #sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options, log_device_placement=False))
        sess = tf.Session()
        with sess.as_default():
            pnet, rnet, onet = align.detect_face.create_mtcnn(sess, None)

    minsize = 20 # minimum size of face
    threshold = [ 0.6, 0.7, 0.7 ]  # three steps's threshold
    factor = 0.709 # scale factor

    # Add a random key to the filename to allow alignment using multiple processes
    random_key = np.random.randint(0, high=99999)
    bounding_boxes_filename = os.path.join(output_dir, 'bounding_boxes_%05d.txt' % random_key)

    with open(bounding_boxes_filename, "w") as text_file:
        nrof_images_total = 0
        nrof_successfully_aligned = 0
        if random_order:
            random.shuffle(dataset)
        for cls in dataset:
            output_class_dir = os.path.join(output_dir, cls.name)
            if not os.path.exists(output_class_dir):
                os.makedirs(output_class_dir)
                if random_order:
                    random.shuffle(cls.image_paths)
            for image_path in cls.image_paths:
                nrof_images_total += 1
                filename = os.path.splitext(os.path.split(image_path)[1])[0]
                output_filename = os.path.join(output_class_dir, filename+'.png')
                print(image_path)
                if not os.path.exists(output_filename):
                    try:
                        img = misc.imread(image_path)
                    except (IOError, ValueError, IndexError) as e:
                        errorMessage = '{}: {}'.format(image_path, e)
                        print(errorMessage)
                    else:
                        if img.ndim<2:
                            print('Unable to align "%s"' % image_path)
                            text_file.write('%s\n' % (output_filename))
                            continue
                        if img.ndim == 2:
                            img = facenet.to_rgb(img)
                        img = img[:,:,0:3]

                        bounding_boxes, _ = align.detect_face.detect_face(img, minsize, pnet, rnet, onet, threshold, factor)
                        nrof_faces = bounding_boxes.shape[0]
                        if nrof_faces>0:
                            det = bounding_boxes[:,0:4]
                            det_arr = []
                            img_size = np.asarray(img.shape)[0:2]
                            if nrof_faces>1:
                                if detect_multiple_faces:
                                    for i in range(nrof_faces):
                                        det_arr.append(np.squeeze(det[i]))
                                else:
                                    bounding_box_size = (det[:,2]-det[:,0])*(det[:,3]-det[:,1])
                                    img_center = img_size / 2
                                    offsets = np.vstack([ (det[:,0]+det[:,2])/2-img_center[1], (det[:,1]+det[:,3])/2-img_center[0] ])
                                    offset_dist_squared = np.sum(np.power(offsets,2.0),0)
                                    index = np.argmax(bounding_box_size-offset_dist_squared*2.0) # some extra weight on the centering
                                    det_arr.append(det[index,:])
                            else:
                                det_arr.append(np.squeeze(det))

                            for i, det in enumerate(det_arr):
                                det = np.squeeze(det)
                                bb = np.zeros(4, dtype=np.int32)
                                bb[0] = np.maximum(det[0]-margin/2, 0)
                                bb[1] = np.maximum(det[1]-margin/2, 0)
                                bb[2] = np.minimum(det[2]+margin/2, img_size[1])
                                bb[3] = np.minimum(det[3]+margin/2, img_size[0])
                                cropped = img[bb[1]:bb[3],bb[0]:bb[2],:]
                                scaled = misc.imresize(cropped, (image_size, image_size), interp='bilinear')
                                nrof_successfully_aligned += 1
                                filename_base, file_extension = os.path.splitext(output_filename)
                                if detect_multiple_faces:
                                    output_filename_n = "{}_{}{}".format(filename_base, i, file_extension)
                                else:
                                    output_filename_n = "{}{}".format(filename_base, file_extension)
                                misc.imsave(output_filename_n, scaled)
                                text_file.write('%s %d %d %d %d\n' % (output_filename_n, bb[0], bb[1], bb[2], bb[3]))
                        else:
                            print('Unable to align "%s"' % image_path)
                            text_file.write('%s\n' % (output_filename))

    print('Total number of images: %d' % nrof_images_total)
    print('Number of successfully aligned images: %d' % nrof_successfully_aligned)


if __name__ == '__main__':
    bucket = 'face_rec.training'
    #Download files from s3 to local for training
    local_path = '/Users/prajwalshreyas/Desktop/Singularity/dockerApps/app_face_rec/app_face_recognition/facenet/facenet_master/src/align/'
    account_email = 'prajwal88@gmail.com'# update as a variable from user account email
    input_dir = local_path
    _start(input_dir, account_email) # download images

    # Run the resizing of the input images
    input_path = local_path+'inputImages/'+account_email+'/inputImages'
    output_dir = local_path+'output_images/'+account_email+'/processedImages/'
    margin = 44
    image_size = 182
    random_order = True
    detect_multiple_faces = False
    main(input_path, output_dir, margin, image_size, random_order, detect_multiple_faces)

    #store the resized images to s3
    upload_s3(output_dir, bucket, account_email)
