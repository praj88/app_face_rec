#Import the OpenCV and dlib libraries
import cv2
import dlib
import logging
import numpy as np
import time
import pandas as pd
import glob
# initialise variables
import os
from flask import Flask, render_template, request, redirect, url_for, send_from_directory
from werkzeug import secure_filename
import boto3 # AWS Packages
from boto3.s3.transfer import S3Transfer # AWS Packages
from config import *
import base64
from io import StringIO, BytesIO
from PIL import Image, ImageFile
import json
from get_face_rec_facenet import *
import pymysql

#Flask api initialise
app = Flask(__name__)

#rds settings
rds_host  = "sqlcam.c3cxjwruoslb.us-east-1.rds.amazonaws.com"
rds_name = db_username
rds_password = db_password
db_name1 = db_name1
db_name2 = db_name2

#s3 settings
s3_transfer = S3Transfer(boto3.client('s3', region_name='us-east-1',aws_access_key_id = AWS_prajwal_credetials['aws_access_key_id'], aws_secret_access_key = AWS_prajwal_credetials['aws_secret_access_key']))

# This is the path to the upload directory
app.config['UPLOAD_FOLDER'] = 'uploads/'
# These are the extension that we are accepting to be uploaded
app.config['ALLOWED_EXTENSIONS'] = set(['txt'])#, 'jpg', 'jpeg'])
#The deisred img_size for face detection
img_size = 64

def download_s3(s3_path, local_path, bucket='face.rec.training'):

    s3 = boto3.client('s3', region_name='us-east-1',aws_access_key_id = AWS_prajwal_credetials['aws_access_key_id'], aws_secret_access_key = AWS_prajwal_credetials['aws_secret_access_key'])
    # resource.meta.client.download_file(bucket, file.get('Key'), dest_pathname)
    s3.download_file(bucket, s3_path, local_path)

# For a given file, return whether it's an allowed type or not
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']

def connect_db(db_name):
    conn = pymysql.connect(rds_host, user=rds_name, passwd=rds_password, db=db_name, connect_timeout=30)
    return conn

def get_train_flag(email_id):
    # Write new log into database --------
    conn = connect_db(db_name2)
    result = None
    try:
        with conn.cursor() as cur:
            cur.execute("SELECT train_flag, timestamp FROM face_rec_train_logs WHERE timestamp= (SELECT max(timestamp) FROM face_rec_train_logs WHERE email = %s)",(email_id))
            result = cur.fetchall()
            conn.close()
    except Exception as e:
            status = e
            conn.close()
    return result

def dlib_face_detection(frame_input):

    # for face detection
    detector = dlib.get_frontal_face_detector()
    dlib_face_locations_track = []


    #detect faces using dlib detector
    detected = detector(frame_input, 1)
    faces_dlib = np.empty((len(detected), img_size, img_size, 3))

    input_img = cv2.cvtColor(frame_input, cv2.COLOR_BGR2RGB)
    img_h, img_w, _ = np.shape(input_img)


    for i, d in enumerate(detected):

        # tracking outputs
        x1, y1, x2, y2, w, h = d.left(), d.top(), d.right() + 1, d.bottom() + 1, d.width(), d.height()
        # generate facelocatios for recognition
        dlib_face_loc_tuple_track = (x1, y1, w, h)
        dlib_face_locations_track.append(dlib_face_loc_tuple_track)


    faces_dlib = dlib_face_locations_track


    return faces_dlib, detected, img_h, img_w

# API set up  ---------------------------------------------------------------------------------------------
@app.route('/face_rec_test')
def face_rec_test():
    return 'Face Detection API working'


@app.route('/face_rec', methods=['POST'])
def face_rec():
    result_all = pd.DataFrame()
    result_all_json = '[{"Names":"No match","Confidence":"0.0"}]'
    if request.method == 'POST':
        logging.warning('Start of Algo')
        result = 'Error in processing, please ensure the format of the file is as per the documentation.'
        # Get the name of the uploaded file
        logging.warning(request)

        try:

            if request.is_json == False: # this section is supporting image file processing

                file = request.files['file']
                print('File Processing')
                if allowed_file(file.filename):
                    logging.warning(file.filename)
                    # Make the filename safe, remove unsupported chars
                    filename = secure_filename(file.filename)
                    filename = file.filename
                    email_id = filename.rsplit('.', 1)[0]
                    filename_img = filename.rsplit('.', 1)[0] + '.jpeg'
                    filepath = 'uploads/' + filename_img
                    logging.warning(filepath)
                    img_data_base64 = file.stream.read()

                    #write image with reduced quality
                    im = Image.open(BytesIO(base64.b64decode(img_data_base64)))
                    im.save(filepath, "JPEG", quality=100, optimize=True, progressive=True)
                    logging.warning('img_data write complete')

                else:
                    result = "Not one of the allowed file extensions. Please try another file."

            elif request.is_json:  # this section is image file in json format processing

                   # read image data or URL
                    img_json = pd.DataFrame(request.json)
                    logging.warning('dataframe read')
                    email_id = img_json['email_id'][0] #image file name
                    img_url_data = img_json['image_data'][0] #image data
                    img_file_name = email_id.split('@')[0]

                    # get train_flag
                    # train_flag_query = get_train_flag('prajwal88@gmail.com')
                    # train_flag =train_flag_query[0][0]
                    # train_flag_date = train_flag_query[0][1]


                    filename_img = img_file_name + '.jpeg'
                    filepath = 'uploads/' + filename_img

                    im = Image.open(BytesIO(base64.b64decode(img_url_data)))
                    im.save(filepath, "JPEG", quality=100, optimize=True, progressive=True)
                    logging.warning('img_data write complete')


            # Upload a image file to S3
            #s3_transfer.upload_file(filepath, 'api.data', str(filename_img))
            #logging.warning('image uploaded to S3')

            # read image from drive
            logging.warning(filepath)
            #filepath = 'images/test/faces/prajwal_2.jpg'
            # im = Image.open(filepath)
            im = cv2.imread(filepath, cv2.IMREAD_COLOR)
            #Resize the image to 320x240
            baseImage = cv2.resize(im, (320, 240))
            faces_dlib, detected_fr, img_h, img_w = dlib_face_detection(baseImage)
            if len(faces_dlib)>0:

                for (x,y,w,h) in faces_dlib:

                    result = get_faceRec(x, y, w, h, baseImage, email_id)
                    #result = json.dumps(result)
                    df_result = pd.DataFrame([result])
                    result_all = result_all.append(df_result)


                # parse to json
                if len(result_all)>0:
                    result_all.columns = ['Names','Confidence']
                    result_all_json = result_all.to_json( orient='records')

                os.remove(filepath)#remove the file
                logging.warning('File delete')
                logging.warning(result_all)
            else:
                result_all_json = '[{"Names":"No face found","Confidence":"0.0"}]'

        except Exception as e:
               logging.warning(e)
               result_all_json = str(e) #'Error while processing the image.'

        return result_all_json




if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True, port=98)
