#Import the OpenCV and dlib libraries
import cv2
import dlib

import numpy as np
import threading
import time
from get_face_rec_facenet import *
import sqlite3
import pandas as pd
import glob
# initialise variables
import os


database = "/app_face_recognition/database/video_metadata.db"
tablename_res = 'video_logs_Dl_frec_20171222'

# Function 1) Create or open database connection
def create_or_open_db(database):
    '''This either creates the main database or opens it'''
    file_exists = os.path.isfile(database)
    conn = sqlite3.connect(database)
    cursor = conn.cursor()
    if file_exists:
        #print ''' "{}" database successfully opened '''.format(filename)
        fix_break =1
    else:
        print(''' "{}" database successfully created '''.format(database))
    return conn


def read_table(conn, table_name_kf, database):

    # picks up the data from the csv and inserts it into a panda dataframe
    cursor = conn.cursor()
    cursor.execute('''SELECT * FROM ''' + table_name_kf)
    table_list = cursor.fetchall()
    table_df = pd.DataFrame(table_list)
    conn.close()

    return table_df

def insert_results(fr_res, tablename_res):

    conn = create_or_open_db(database)

    # insert data to sql
    fr_res.to_sql(tablename_res, conn, index = False, if_exists='append')

    conn.commit()
    conn.close()


#Initialize a face cascade using the frontal face haar cascade provided with
#the OpenCV library
#Make sure that you copy this file from the opencv project to the root of this
#project folder

#The deisred img_size for face detection
img_size = 64

def dlib_face_detection(frame_input):

    # for face detection
    detector = dlib.get_frontal_face_detector()
    dlib_face_locations_track = []


    #detect faces using dlib detector
    detected = detector(frame_input, 1)
    faces_dlib = np.empty((len(detected), img_size, img_size, 3))

    input_img = cv2.cvtColor(frame_input, cv2.COLOR_BGR2RGB)
    img_h, img_w, _ = np.shape(input_img)


    for i, d in enumerate(detected):

        # tracking outputs
        x1, y1, x2, y2, w, h = d.left(), d.top(), d.right() + 1, d.bottom() + 1, d.width(), d.height()
        # generate facelocatios for recognition
        dlib_face_loc_tuple_track = (x1, y1, w, h)
        dlib_face_locations_track.append(dlib_face_loc_tuple_track)


    faces_dlib = dlib_face_locations_track


    return faces_dlib, detected, img_h, img_w



def detectAndTrackMultipleFaces():

    #size = (1920, 1080)
    size = (1280, 720)
    #size = (800, 450)
    video_out = True



    # Create an output movie file (make sure resolution/frame rate matches input video!)
    if video_out:
        fourcc = cv2.VideoWriter_fourcc(*'XVID')
        output_movie = cv2.VideoWriter('/app_face_recognition/videos/2018-01-01/output/cam1_2018-01-01_out_fn_track_age.avi', fourcc, 10, size)


    for file in glob.glob("/app_face_recognition/videos/2018-01-01/cam1_*.mp4"):
        print(file)

        # append logs to dataframe
        fr_result_all = pd.DataFrame()

        fr_result = pd.DataFrame({'ObjectDetected':[],'FrameNumber':[],'NumObjects':[],
                                'FrameObjectNum':[],'Label':[],'Confidence':[],'BottomRight_x':[],
                                'BottomRight_y':[],'TopLeft_x':[],'TopLeft_y':[], 'Age':[], 'Gender':[]})


        # get file names and loop through each file name
        input_movie_path = file
        input_movie_file_name = os.path.basename(input_movie_path)



        #Open the video files device
        capture = cv2.VideoCapture(input_movie_path)
        length = int(capture.get(cv2.CAP_PROP_FRAME_COUNT))

        width = capture.get(3)  # float
        height = capture.get(4) # float

        #The color of the rectangle we draw around the face
        rectangleColor = (0,165,255)

        #variables holding the current frame number and the current faceid
        frameCounter = 0
        currentFaceID = 0
        rec_flag=False

        #Variables holding the correlation trackers and the name per faceid
        faceTrackers = {}
        faceNames = {}
        fr_flag_all = {}

        while True:
            #Retrieve the latest image from the webcam
            ret, fullSizeBaseImage = capture.read()

            # Quit when the input video file ends
            if not ret:
                break

            #Resize the image to 320x240
            baseImage = cv2.resize(fullSizeBaseImage, size)

            #Result image is the image we will show the user, which is a
            #combination of the original image from the webcam and the
            #overlayed rectangle for the largest face
            resultImage = baseImage.copy()



            #STEPS:
            # * Update all trackers and remove the ones that are not
            #   relevant anymore
            # * Every 10 frames:
            #       + Use face detection on the current frame and look
            #         for faces.
            #       + For each found face, check if centerpoint is within
            #         existing tracked box. If so, nothing to do
            #       + If centerpoint is NOT in existing tracked box, then
            #         we add a new tracker with a new face-id


            #Increase the framecounter
            frameCounter += 1



            #Update all the trackers and remove the ones for which the update
            #indicated the quality was not good enough
            fidsToDelete = []
            for fid in faceTrackers.keys():
                trackingQuality = faceTrackers[ fid ].update( baseImage )

                #If the tracking quality is good enough, we must delete
                #this tracker
                if trackingQuality < 5:
                    fidsToDelete.append( fid )


            for fid in fidsToDelete:
                print("Removing fid " + str(fid) + " from list of trackers")
                faceTrackers.pop( fid , None )
                fr_flag_all.pop(fid , None )




            #Every 10 frames, we will have to determine which faces
            #are present in the frame
            if (frameCounter % 10) == 0:



                #For the face detection, we need to make use of a gray
                #colored image so we will convert the baseImage to a
                #gray-based image
                gray = cv2.cvtColor(baseImage, cv2.COLOR_BGR2GRAY)
                #Now use the dlib face detector to find all faces
                #in the image
                faces_dlib, detected_fr, img_h, img_w = dlib_face_detection(baseImage)


                #Loop over all faces and check if the area for this
                #face is the largest so far
                #We need to convert it to int here because of the
                #requirement of the dlib tracker. If we omit the cast to
                #int here, you will get cast errors since the detector
                #returns numpy.int32 and the tracker requires an int
                for (_x,_y,_w,_h) in faces_dlib: # faces:
                    x = int(_x)
                    y = int(_y)
                    w = int(_w)
                    h = int(_h)
                    faceArea = w*h
                    #print ("x:{}, y:{},w:{}, h:{}".format(x, y, w, h))


                    #calculate the centerpoint
                    x_bar = x + 0.5 * w
                    y_bar = y + 0.5 * h



                    #Variable holding information which faceid we
                    #matched with
                    matchedFid = None

                    #Now loop over all the trackers and check if the
                    #centerpoint of the face is within the box of a
                    #tracker
                    for fid in faceTrackers.keys():

                        tracked_position =  faceTrackers[fid].get_position()

                        t_x = int(tracked_position.left())
                        t_y = int(tracked_position.top())
                        t_w = int(tracked_position.width())
                        t_h = int(tracked_position.height())


                        #calculate the centerpoint
                        t_x_bar = t_x + 0.5 * t_w
                        t_y_bar = t_y + 0.5 * t_h

                        #check if the centerpoint of the face is within the
                        #rectangleof a tracker region. Also, the centerpoint
                        #of the tracker region must be within the region
                        #detected as a face. If both of these conditions hold
                        #we have a match
                        if ( ( t_x <= x_bar   <= (t_x + t_w)) and
                             ( t_y <= y_bar   <= (t_y + t_h)) and
                             ( x   <= t_x_bar <= (x   + w  )) and
                             ( y   <= t_y_bar <= (y   + h  ))):
                            matchedFid = fid

                        if len(fr_flag_all)>0:
                            if fr_flag_all[fid]==False and matchedFid == fid:
                                print ('Re check for face rec')
                                #print([t_x, t_y, t_w, t_h])
                                fr_out_name_all, fr_out_confidence_all, fr_flag= get_faceRec(x, y, w, h, img_h, img_w, baseImage, fid, faceArea)
                                fr_flag_all.update(fr_flag)
                                #print ("fid: {}, fr_out_name_all: {}".format(fid, fr_out_name_all.keys()))


                    #If no matched fid, then we have to create a new tracker
                    if (matchedFid is None):

                        print("Creating new tracker " + str(currentFaceID))

                        #Create and store the tracker
                        tracker = dlib.correlation_tracker()
                        tracker.start_track(baseImage,
                                            dlib.rectangle( x-5,
                                                            y-10,
                                                            x+w+5,
                                                            y+h+10))

                        faceTrackers[ currentFaceID ] = tracker

                    #<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Start face recognition >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                        fr_out_name_all, fr_out_confidence_all, fr_flag = get_faceRec(x, y, w, h, img_h, img_w, baseImage, currentFaceID, faceArea)
                        print("frame {}, currentFaceID {}, fr_flag {} ".format(frameCounter, currentFaceID, fr_flag.keys()))
                        fr_flag_all.update(fr_flag)
                        print(fr_flag_all)
                        #Increase the currentFaceID counter
                        currentFaceID += 1




            # Now loop over all the trackers we have and draw the rectangle
            # around the detected faces. If we 'know' the name for this person
            # (i.e. the recognition thread is finished), we print the name
            # of the person, otherwise the message indicating we are detecting
            # the name of the person
            for fid in faceTrackers.keys():

                tracked_position =  faceTrackers[fid].get_position()

                t_x = int(tracked_position.left())
                t_y = int(tracked_position.top())
                t_w = int(tracked_position.width())
                t_h = int(tracked_position.height())

                print ("Image Area: {}".format(t_w*t_h))

                cv2.rectangle(resultImage, (t_x, t_y),
                                        (t_x + t_w , t_y + t_h),
                                        rectangleColor ,2)

                #print ("fid {}, fr_out_all: {}".format(fid, fr_out_all.keys()))

                if fid in fr_out_name_all.keys():
                    fr_res = fr_out_name_all[fid] + " (" + str(fr_out_confidence_all[fid]) +")"

                    # colour of text is set as red only if the d=face recognition is successful
                    if fr_out_name_all[fid] == "Detecting":
                        textColor = (0,165,255)
                    else:
                        textColor = (0,0,255)

                    cv2.putText(resultImage, fr_res ,
                                (int(t_x - t_w/2 ), int(t_y)),
                                cv2.FONT_HERSHEY_SIMPLEX,
                                0.5, textColor, 2)

                    frame_result = {'ObjectDetected': 1,
                                    'FrameNumber':frameCounter,
                                    'NumObjects':1,
                                    'FrameObjectNum':0,
                                    'Label':fr_out_name_all[fid],
                                    'Confidence':fr_out_confidence_all[fid],
                                    'BottomRight_x':t_x,
                                    'BottomRight_y':t_y,
                                    'TopLeft_x':t_x + t_w,
                                    'TopLeft_y':t_y + t_h,
                                    'Age':0,
                                    'Gender':'NA',
                                    }

                else:
                   cv2.putText(resultImage, "Detecting",
                               (int(t_x - t_w/2), int(t_y)),
                               cv2.FONT_HERSHEY_SIMPLEX,
                               0.5, (0,165,255), 2)

                   frame_result = {'ObjectDetected': 1,
                                    'FrameNumber':frameCounter,
                                    'NumObjects':1,
                                    'FrameObjectNum':0,
                                    'Label':'Detecting',
                                    'Confidence': 0,
                                    'BottomRight_x':t_x,
                                    'BottomRight_y':t_y,
                                    'TopLeft_x':t_x + t_w,
                                    'TopLeft_y':t_y + t_h,
                                    'Age':0,
                                    'Gender':'NA',
                                    }

                fr_result = fr_result.append(frame_result, ignore_index=True)


            #Finally, we want to show the images on the screen
            if video_out:
                output_movie.write(resultImage)
            print("Writing frame {} / {}".format(frameCounter, length))


        # file metadata to insert into sql
        file_metadata = input_movie_file_name.split('_')

        camera_name = file_metadata[0]
        file_date = file_metadata[1]
        file_hour = int(file_metadata[2])
        file_min = int(file_metadata[3])
        file_sec = int(file_metadata[4][:2])


        fr_result['FrameWidth']  = width
        fr_result['FrameHeight'] = height
        fr_result['CameraName'] = camera_name
        fr_result['Date'] = file_date
        fr_result['Hour'] = file_hour
        fr_result['Minute'] = file_min
        fr_result['Second'] = file_sec

        fr_result_all = fr_result_all.append(fr_result, ignore_index=True)

        # insert face recognition data to sql db
        #insert_results(fr_result_all, tablename_res)
        #print('Inserted into DB')






if __name__ == '__main__':
    detectAndTrackMultipleFaces()
