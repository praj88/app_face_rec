FROM waleedka/modern-deep-learning

# File Author / Maintainer
MAINTAINER Prajwal Shreyas

# Copy the application folder inside the container
ADD /app_face_recognition /app_face_recognition

# install jupyter Kernel gateway
#RUN pip install jupyter_kernel_gateway
RUN pip install dlib
RUN pip install pymysql


# Get pip to download and install requirements:
RUN pip install -r /app_face_recognition/requirements.txt

# Expose ports
#EXPOSE 8000
EXPOSE 98

# Set the default directory where CMD will execute
WORKDIR /app_face_recognition
CMD python3 face_rec.py
